# Week 4 Project: Containerize a Rust Actix Web Service

## Part I. Create a Rust Actix Web App

### 1. Install Rust and Cargo

### 2. Build a new Rust project
Open a terminal and run `cargo new week4proj --bin` to create a new binary project.

### 3. Add Actix-web to the dependencies
Open `Cargo.toml` and add `actix-web` under `[dependencies]` like so:
```
[dependencies]
actix-web = "4.0"
```

### 4. Create a simple web server
Edit `src/main.rs` to set up a basic web server.
```
use actix_web::{web, App, HttpResponse, HttpServer, Responder};

async fn greet() -> impl Responder {
    HttpResponse::Ok().body("Hello, world!")
}

#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| App::new().route("/", web::get().to(greet)))
        .bind("0.0.0.0:8080")?
        .run()
        .await
}
```

### 5. Test the application
Run `cargo run` to start the server.
You may see the following statements from the terminal:
```
    Finished dev [unoptimized + debuginfo] target(s) in 0.54s
     Running `target/debug/week4proj`

```
Visit `http://0.0.0.0:8080` in the browser to see if "Hello, world!" is displayed.

![url](https://gitlab.com/estrellas939/week4proj/-/raw/main/pic/url.png?inline=false)

## Part II. Containerize the Rust Actix Web App

### 6. Create a Dockerfile

In the root of the Rust project, create a file named Dockerfile.
Here’s my Dockerfile for the Rust application:
```
# Phase 1: Build
FROM rust:1.75 as builder
WORKDIR /usr/src/myapp

# Copy source code
COPY ./ ./

# Install necessary tools and add musl target
RUN apt-get update && apt-get install -y musl-tools
RUN rustup target add x86_64-unknown-linux-musl

# Build statically linked binaries for Alpine
RUN cargo build --release --target x86_64-unknown-linux-musl

# Second stage: run
FROM alpine:latest
WORKDIR /root/

#Install runtime dependencies
RUN apk --no-cache add ca-certificates

# Copy the built app from the builder
COPY --from=builder /usr/src/myapp/target/x86_64-unknown-linux-musl/release/week4proj .

# Run application
CMD ["./week4proj"]
```
This Dockerfile uses a multi-stage build to keep the final image as small and secure as possible.

### 7. Build the Docker image

Run `docker build -t week4proj .` to create an image of the app.
Here is the result from terminal:
```
[+] Building 110.6s (16/16) FINISHED                       docker:desktop-linux
 => [internal] load build definition from Dockerfile                       0.1s
 => => transferring dockerfile: 740B                                       0.0s
 => [internal] load metadata for docker.io/library/alpine:latest           2.1s
 => [internal] load metadata for docker.io/library/rust:1.75               2.1s
 => [internal] load .dockerignore                                          0.0s
 => => transferring context: 2B                                            0.0s
 => [builder 1/6] FROM docker.io/library/rust:1.75@sha256:87f3b2f93b82995  0.0s
 => [stage-1 1/4] FROM docker.io/library/alpine:latest@sha256:c5b1261d6d3  0.0s
 => [internal] load build context                                          0.2s
 => => transferring context: 229.29kB                                      0.2s
 => CACHED [stage-1 2/4] WORKDIR /root/                                    0.0s
 => CACHED [stage-1 3/4] RUN apk --no-cache add ca-certificates            0.0s
 => CACHED [builder 2/6] WORKDIR /usr/src/myapp                            0.0s
 => [builder 3/6] COPY ./ ./                                               5.7s
 => [builder 4/6] RUN apt-get update && apt-get install -y musl-tools      5.6s
 => [builder 5/6] RUN rustup target add x86_64-unknown-linux-musl         10.0s 
 => [builder 6/6] RUN cargo build --release --target x86_64-unknown-linu  86.6s 
 => [stage-1 4/4] COPY --from=builder /usr/src/myapp/target/x86_64-unknow  0.1s 
 => exporting to image                                                     0.1s 
 => => exporting layers                                                    0.1s 
 => => writing image sha256:342871477748359d27b24d60edf5bcc862dd2ffa1d6aa  0.0s 
 => => naming to docker.io/library/week4proj                               0.0s 

View build details: docker-desktop://dashboard/build/desktop-linux/desktop-linux/t90k6afm854cxvg9or8kgnspi

What's Next?
  View a summary of image vulnerabilities and recommendations → docker scout quickview
```
![image_list](https://gitlab.com/estrellas939/week4proj/-/raw/main/pic/image_list.png?inline=false)
![image](https://gitlab.com/estrellas939/week4proj/-/raw/main/pic/image.png?inline=false)
### 8. Run the container locally:

Once the build is complete, run the container using `docker run -p 8080:8080 week4proj`.

![container_list](https://gitlab.com/estrellas939/week4proj/-/raw/main/pic/container_list.png?inline=false)
![container_running](https://gitlab.com/estrellas939/week4proj/-/raw/main/pic/container.png?inline=false)



